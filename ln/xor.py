# import sys

# sys.stdin = open("tobin.in", "r")
# sys.stdout = open("tobin.out", "w")


def d2b(x):
  return bin(x)[2:]


s = "2 3 4 4 2"
sp = s.split()  # ['2', '3'...]
print(sp)
a = list(map(int, sp))  # [2, 3, 4, 4, 2]
print(a)
result = 0
print("0", end='')
for x in a:
  result ^= x
  # print('^' + str(x) + '=' + str(result), end='')
  print('^' + d2b(x) + '=' + d2b(result), end='')

print()
print(result)  # 3
