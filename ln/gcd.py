def gcd(a, b):
  print("(" + str(a) + ", " + str(b) + ")", end=' \\rightarrow ')
  return a if b == 0 else gcd(b, a % b)


def gcd2(a, b, aa, ba, ab, bb):  # a in a, b in a
  # print("(" + str(a) + ", " + str(b) + ")", end=' \\rightarrow ')
  if bb >= 0:
    print(f"  {b} = {ab}a + {bb}b", end=' ')
  else:
    print(f"  {b} = {ab}a {bb}b", end=' ')

  if b != 0:
    d = a // b
    print(f" \\times {d}")
  else:
    print()
  print()
  return a, aa, ba if b == 0 else gcd2(b, a % b, ab, bb, aa - d * ab, ba - d * bb)


# a in a, a in b, b in a, b in b
def diophantine(a, b, aa, ab, ba, bb):
  return (a, aa, ba) if b == 0 else \
    diophantine(b, a % b,
                ab, aa - a // b * ab,
                bb, ba - a // b * bb)


def GCD2(a, b):
  print(f"  a = {a}, b = {b}")
  print()
  print(f"  {a} = 1a + 0b")
  print()
  gcd2(a, b, 1, 0, 0, 1)
  print(diophantine(a, b, 1, 0, 0, 1))
  print()
  print()


print()


def GCD(a, b):
  print("  ", end='')
  print(gcd(a, b))
  print()


GCD(15, 10)
GCD(69, 45)

GCD((2 ** 10) * (3 ** 1), (2 ** 1) * (3 ** 6))

print(GCD2(15, 10))
print(GCD2(69, 45))
GCD2((2 ** 10) * (3 ** 1), (2 ** 1) * (3 ** 6))
